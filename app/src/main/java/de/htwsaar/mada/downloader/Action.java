package de.htwsaar.mada.downloader;

final class Action {

    static final String PUBLISH_PROGRESS = "de.htwsaar.mada.downloader.action.PUBLISH_PROGRESS";
    static final String START_DOWNLOAD = "de.htwsaar.mada.downloader.action.START_DOWNLOAD";
    static final String DOWNLOAD_STARTED = "de.htwsaar.mada.downloader.action.DOWNLOAD_STARTED";
    static final String DOWNLOAD_REJECTED = "de.htwsaar.mada.downloader.action.DOWNLOAD_REJECTED";

    private Action() {

    }
}
