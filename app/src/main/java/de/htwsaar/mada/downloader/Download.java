package de.htwsaar.mada.downloader;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;

final class Download implements Parcelable {

    private static final String FILENAME_REGEX = "[^\\\\/\\s]+";

    /**
     * Conveniently create a download from a URL.
     * @param url url of the file to download
     * @return
     */
    static Download fromUrl(final URL url) {
        final long id = System.nanoTime();
        String[] splitPath = url.getFile().split("/");
        String filename = (splitPath.length > 0 ? splitPath[splitPath.length - 1] : "");
        if (!filename.matches(FILENAME_REGEX)) {
            // if we don't get a valid filename (i.e. when downloading
            // an implicit index.html or a mod-rewrite url)
            // create a filename from the id.
            filename = "downloaded file " + id;
        }
        return new Download(id, url, filename);
    }

    private final long id;
    private final URL url;
    private final String filename;

    Download(long id, URL url, String filename) {
        this.id = id;
        this.url = url;
        this.filename = filename;
    }

    long getId() {
        return id;
    }

    URL getUrl() {
        return url;
    }

    String getFilename() {
        return filename;
    }

    public static final Creator<Download> CREATOR = new Creator<Download>() {
        @Override
        public Download createFromParcel(final Parcel source) {
            try {
                return new Download(
                        source.readLong(),
                        new URL(source.readString()),
                        source.readString()
                );
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public Download[] newArray(int size) {
            return new Download[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeLong(id);
        dest.writeString(url.toString());
        dest.writeString(filename);
    }
}
