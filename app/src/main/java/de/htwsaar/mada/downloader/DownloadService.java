package de.htwsaar.mada.downloader;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class DownloadService extends Service {

    private Download mCurrentDownload;
    private DownloadTask mDownloadTask;
    private volatile boolean mDownloadRunning = false;

    public DownloadService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mCurrentDownload = null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mDownloadRunning) {
            // Notify the app that the service hasn't finished downloading yet
            sendBroadcast(new Intent(Action.DOWNLOAD_REJECTED));
        } else if (intent == null) {
            Log.d("DownloadService", "onStartCommand(): intent was null");
        } else {
            mCurrentDownload = intent.getParcelableExtra(Extra.DOWNLOAD);
            mDownloadTask = new DownloadTask(mCurrentDownload);

            // Move the service to the foreground, so the system doesn't kill it when the app dies.
            startForeground(0xf00, buildForegroundNotification(mCurrentDownload.getFilename()));

            // Notify the app that the download has started via broadcast
            final Intent started = new Intent(Action.DOWNLOAD_STARTED);
            started.putExtra(Extra.DOWNLOAD_FILENAME, mCurrentDownload.getFilename());
            sendBroadcast(new Intent(Action.DOWNLOAD_STARTED));

            mDownloadTask.start();
            mDownloadRunning = true;
        }
        return START_STICKY;
    }

    private Notification buildForegroundNotification(final String filename) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        builder.setOngoing(true)
                .setContentTitle(getString(R.string.downloading) + mCurrentDownload.getFilename())
                .setContentText(filename)
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setTicker(getString(R.string.downloading));

        return(builder.build());
    }

    @Override
    public IBinder onBind(final Intent intent) {
        // We don't allow binding
        return null;
    }

    private synchronized void publishProgress(double progress) {
        Log.d("DownloadService", "Progress at " + progress);
        final Intent intent = new Intent(Action.PUBLISH_PROGRESS);
        intent.putExtra(Extra.DOWNLOAD_FILENAME, mCurrentDownload.getFilename());
        intent.putExtra(Extra.DOWNLOAD_PROGRESS, progress);
        sendBroadcast(intent);
    }

    private synchronized void finishDownload() {
        mDownloadRunning = false;

        // Move the service to the background, so the system can kill it if necessary
        stopForeground(true);
    }

    private File getDownloadsDir() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    }

    private final class DownloadTask extends Thread {

        private static final long CHUNK_SIZE = 1 << 12; // 2^12 bytes = 4KiB

        private final Download mDownload;

        DownloadTask(final Download download) {
            mDownload = download;
        }

        @Override
        public void run() {
            final File file = new File(getDownloadsDir(), mDownload.getFilename());
            Log.d("DownloadService", file.getAbsolutePath());
            try {
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
            } catch (IOException e) {
                Log.d("DownloadService", "exception", e);
                return;
            }
            try (
                    InputStream in = mDownload.getUrl().openStream();
                    ReadableByteChannel rbc = Channels.newChannel(in);
                    FileOutputStream fos = new FileOutputStream(file)
            ) {
                final long total = mDownload.getUrl().openConnection().getContentLength();
                long count = 0;
                long bytesRead;
                do {
                    bytesRead = fos.getChannel().transferFrom(rbc, count, CHUNK_SIZE);
                    count += bytesRead;
                    // Notify the app of the progress
                    publishProgress(((double) count) / total);
                } while (count < total);
                fos.flush();
                fos.close();
                finishDownload();
            } catch (IOException e) {
                Log.d("DownloadService", "exception", e);
            }
        }
    }
}
