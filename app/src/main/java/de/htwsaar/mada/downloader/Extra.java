package de.htwsaar.mada.downloader;

final class Extra {

    static final String DOWNLOAD_PROGRESS = "de.htwsaar.mada.downloader.extra.DOWNLOAD_PROGRESS";
    static final String DOWNLOAD = "de.htwsaar.mada.downloader.extra.DOWNLOAD";
    static final String DOWNLOAD_FILENAME = "de.htwsaar.mada.downloader.extra.DOWNLOAD_FILENAME";

    private Extra() {

    }
}
