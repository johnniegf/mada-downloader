package de.htwsaar.mada.downloader;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText txtUrl;
    private TextView txtFile;
    private ProgressBar progressBar;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction() == null ? "" : intent.getAction();

            switch (action) {
                case Action.DOWNLOAD_STARTED:
                    progressBar.setProgress(0);
                    txtFile.setText(intent.getStringExtra(Extra.DOWNLOAD_FILENAME));
                    txtFile.invalidate();
                    txtFile.requestLayout();
                    showToast(R.string.downloadStarted);
                    break;
                case Action.DOWNLOAD_REJECTED:
                    showToast(R.string.downloadRejected);
                    break;
                case Action.PUBLISH_PROGRESS:
                    txtFile.setText(intent.getStringExtra(Extra.DOWNLOAD_FILENAME));
                    txtFile.invalidate();
                    txtFile.requestLayout();

                    final double progress = intent.getDoubleExtra(Extra.DOWNLOAD_PROGRESS, -1);
                    if (progress >= 0) {
                        progressBar.setProgress((int) Math.round(1000 * progress));
                    }
                    break;
            }
        }
    };

    private IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Request all necessary permissions
        // Because declaring them in the manifest is not enough for some reason...
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.READ_EXTERNAL_STORAGE},
                10);

        progressBar = findViewById(R.id.downloadProgress);
        progressBar.setMax(1000);
        txtUrl = findViewById(R.id.downloadUrl);
        txtFile = findViewById(R.id.downloadFilename);

        filter = new IntentFilter();
        filter.addAction(Action.DOWNLOAD_STARTED);
        filter.addAction(Action.DOWNLOAD_REJECTED);
        filter.addAction(Action.PUBLISH_PROGRESS);
        registerReceiver(receiver, filter);
    }

    private void showToast(@StringRes final int stringRes) {
        Toast.makeText(this, stringRes, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(receiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    private void startDownload(final String urlString) {
        try {
            final URL url = new URL(urlString);
            final Download dl = Download.fromUrl(url);
            final Intent dlService = new Intent(this, DownloadService.class);
            dlService.setAction(Action.START_DOWNLOAD);
            dlService.putExtra(Extra.DOWNLOAD, dl);
            startService(dlService);
        } catch (MalformedURLException e) {
            Toast.makeText(this, R.string.errMalformedUrl, Toast.LENGTH_LONG).show();
        }
    }

    public void onStartButtonClicked(final View v) {
        final String urlString = txtUrl.getText().toString();
        startDownload(urlString);
    }
}
